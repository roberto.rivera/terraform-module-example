TERRAFORM-MODULE-EXAMPLE
========================

## Intruduction

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris semper suscipit elementum. Donec convallis leo nisl, eu pharetra arcu egestas et. Nulla at purus leo. In sed nisi non dui pulvinar ultricies at eget eros. Sed odio nibh, egestas non vulputate sed, aliquet et velit. Sed ac est in justo luctus tristique. Morbi posuere ac diam vitae congue. Suspendisse cursus consectetur pulvinar. Vivamus lacinia risus nisi, eu lobortis nunc sodales eget. Nullam rutrum urna mi, sit amet consequat lacus rhoncus id. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vitae nisi vitae odio commodo euismod at at arcu. Duis nibh nisi, tincidunt at mollis nec, laoreet nec metus. In faucibus pellentesque felis. Quisque eu odio dignissim, congue justo tempor, scelerisque odio.

Integer pellentesque, lorem venenatis hendrerit posuere, lectus elit luctus magna, vulputate mollis orci tortor quis justo. Sed sed feugiat purus, ut volutpat erat. In vel rhoncus est, non egestas nisl. Integer dignissim quis tortor sed lacinia. Nullam porttitor sem diam, at fermentum nulla accumsan sit amet. Ut sit amet sapien eu ex placerat rutrum quis sit amet neque. Sed egestas odio est, quis facilisis quam tempor sit amet. Vivamus consequat ligula quis fermentum eleifend. Pellentesque luctus aliquam neque sed pretium. In eros metus, cursus sed vestibulum eu, elementum eget nisi. Integer vel pharetra est, id condimentum velit. Sed lacinia, lectus et facilisis elementum, est lectus tristique tellus, bibendum convallis nisl massa vel leo. Proin a lobortis mi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

## Usage:

```hcl
# some comment

provider "aws" {
  region = "us-west-2"
}

# this calls a module that will create a role
module "role" {
  source = "s3://rrivera-terraform-modules/terraform-module-example/terraform-module-example.1.9.4.zip"

  role_name = "modules-demo"
}

# create an output
output "role_name" {
  value = module.role.role_name
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| role\_name | the role name for the web server | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| role\_arn | the role's arn |
| role\_id | the role's id |
| role\_name | the role's name |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
